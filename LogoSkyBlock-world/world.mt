gameid = minetest
load_mod_3d_armor_quests = false
load_mod_asteroid = false
load_mod_defense = false
load_mod_ethereal_quests = false
load_mod_marssurvive = false
load_mod_meru = false
load_mod_minetest_quests = false
load_mod_moonrealm = false
load_mod_mymonths = false
load_mod_phonics = false
load_mod_protector = false
load_mod_quests = false
load_mod_realms = false
load_mod_regeneration = false
load_mod_sample_quest = false
load_mod_skyblock = false
load_mod_skybox = true
load_mod_skybox_copy = false
load_mod_survival = false
load_mod_sys4_quests = false
load_mod_throwing = false
load_mod_tutor = false
load_mod_watershed = false
backend = sqlite3
